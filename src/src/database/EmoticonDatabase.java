package src.database;

import src.model.EmoticonModel;

import javax.ejb.Stateless;
import java.util.LinkedList;
import java.util.List;

@Stateless
public class EmoticonDatabase {
    private List emoticonList = new LinkedList();

    public List<EmoticonModel> getEmoticonList() {
        return emoticonList;
    }
}
