package src.model;

import java.io.File;

public class EmoticonModel {

    private File file;
    private String image;

    public EmoticonModel(File file, String image) {
        this.file = file;
        this.image = image;
    }

    public File getFile() {
        return file;
    }

    public String getImage() {
        return image;
    }
}
