package src.service;

import src.database.EmoticonDatabase;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;


@Stateless
public class ImageService {

    @EJB
    private EmoticonDatabase emoticonDatabase;

    private OutputStream outputStream = null;
    private int counter = 0;

    public String basicView() {
        if (!isEmpty()) {
            return emoticonDatabase.getEmoticonList().get(counter).getImage();
        } else {
            return null;
        }
    }

    public void nextView() {
        if (hasNext()) {
            counter++;
        }
    }

    public void previousView() {
        if (hasPrevious()) {
            counter--;
        }
    }

    public void delete() {
        if (hasPrevious()) {
            counter--;
            emoticonDatabase.getEmoticonList().remove(counter + 1);
            counter = 0;
        } else if (hasNext()) {
            counter++;
            emoticonDatabase.getEmoticonList().remove(counter - 1);
            counter = 0;
        } else {
            if (!isEmpty())
                emoticonDatabase.getEmoticonList().remove(counter);
            counter = 0;
        }
    }

    public void download() {
        if (!isEmpty()) {
            File file = emoticonDatabase.getEmoticonList().get(counter).getFile();
            try {
                byte[] fileToWrite = Files.readAllBytes(file.toPath());
                outputStream = new FileOutputStream("C:\\Download\\" + file.getName());
                outputStream.write(fileToWrite);
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean hasNext() {
        if (counter < emoticonDatabase.getEmoticonList().size() - 1) {
            return true;
        } else {
            return false;
        }
    }

    private boolean hasPrevious() {
        if (counter >= 1) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isEmpty() {
        if (emoticonDatabase.getEmoticonList().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}
