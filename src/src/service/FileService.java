package src.service;

import src.database.EmoticonDatabase;
import src.model.EmoticonModel;
import src.service.mapper.ImageMapper;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.File;

@Stateless
public class FileService {

    @EJB
    private ImageMapper imageMapper;

    @EJB
    private EmoticonDatabase emoticonDatabase;

    public boolean addFileToDB(File file) {
        if (file.getName().endsWith(".jpg")) {
            String image = imageMapper.fileToString(file);
            if (image == null)
                return false;
            emoticonDatabase.getEmoticonList().add(new EmoticonModel(file, image));
            return true;
        } else {
            return false;
        }
    }
}
