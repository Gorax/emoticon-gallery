package src.service.mapper;

import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

@Stateless
public class ImageMapper {
    private BufferedImage image;

    public String fileToString(File file) {
        try {
            image = ImageIO.read(file);

            if (image == null)
                return null;

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", baos);

            String data = DatatypeConverter.printBase64Binary(baos.toByteArray());
            String imageString = "data:image/png;base64," + data;

            return imageString;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
