package src.controllers;

import org.apache.commons.io.FileUtils;
import src.service.FileService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;


@WebServlet("/loadEmoticon")
@MultipartConfig
public class LoadFileController extends HttpServlet {

    @EJB
    private FileService fileService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part filePart = req.getPart("file");
        InputStream fileContent = filePart.getInputStream();
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        File file = new File(fileName);

        try {
            FileUtils.copyInputStreamToFile(fileContent, file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (fileService.addFileToDB(file)) {
            resp.sendRedirect(resp.encodeRedirectURL("home.jsp?success"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL("home.jsp?invalidData"));
        }
    }
}
