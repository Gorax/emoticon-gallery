package src.controllers;

import src.service.ImageService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/emoticonGallery")
public class EmoticonGalleryController extends HttpServlet {

    @EJB
    private ImageService imageService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("next") != null) {
            imageService.nextView();
            basicView(req,resp);
        } else if (req.getParameter("previous") != null) {
            imageService.previousView();
            basicView(req,resp);
        } else if (req.getParameter("delete") != null) {
            imageService.delete();
            basicView(req,resp);
        } else if (req.getParameter("download") != null){
            imageService.download();
            basicView(req,resp);
        }else{
            basicView(req,resp);
        }
    }

    private void basicView (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("image", imageService.basicView());
        req.getRequestDispatcher("emoticonGallery.jsp").forward(req, resp);
    }
}
