<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Emoticon gallery</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-item.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="home.jsp">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="emoticonGallery">Emoticon gallery</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-3">
            <h1 class="my-4">Emoticon Application
            </h1>

            <div class="list-group">
                <a class="list-group-item">Choose one of the following actions:</a>
            </div>

            <div class="list-group">
                <a href="emoticonGallery?next" class="list-group-item">next view</a>
            </div>

            <div class="list-group">
                <a href="emoticonGallery?previous" class="list-group-item">previous view</a>
            </div>

            <div class="list-group">
                <a href="emoticonGallery?delete" class="list-group-item">delete emoticon</a>
            </div>

            <div class="list-group">
                <a href="emoticonGallery?download" class="list-group-item">download emoticon</a>
            </div>
        </div>


        <div class="col-lg-9">
            <div class="card card-outline-secondary my-4">
                <div class="card-header">
                    Your view:
                </div>
                <div class="card-body">

                    <%
                        String image = (String) request.getAttribute("image");
                        if (image != null && !image.isEmpty()) {
                    %>
                    <img class="card-img-top img-fluid" src="<%=image%>" alt="Something went wrong">
                    <%
                    } else {
                    %>
                    <a>Your gallery is empty.</a>
                    <%
                        }
                    %>
                    <%--<img class="card-img-top img-fluid" src="" alt="cos poszlo nie tak">--%>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-lg-9 -->
    </div>

</div>

</body>

</html>
